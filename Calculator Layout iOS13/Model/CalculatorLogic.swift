//
//  CalculatorLogic.swift
//  Calculator Layout iOS13
//
//  Created by Lorin Sherry on 07/04/2022.
//  Copyright © 2022 The App Brewery. All rights reserved.
//

import Foundation

struct CalculatorLogic {
    
    private var number: Double?
    
    private var intermediateCalculation (n1: Double, calcMethod: String)?
    
    mutating func setNumber(_ number: Double) {
        self.number = number
    }
    
    mutating func calculate(symbol: String) -> Double? {
        if let n = number {
            switch symbol {
                case
            }
        }
    }
    
    init(number: Double){
        self.number = number
    }
    
    func calculate(symbol: String) -> Double? {
        
            if symbol == "+/-" {
                return number * -1
            } else if symbol == "%" {
                return number / 100
            } else if symbol == "AC" {
                return 0
            }
        return nil
    }
    
}
